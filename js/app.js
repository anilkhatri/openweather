'use strict';

// Declare app level module which depends on filters, and services
angular.module('weatherApp', [
  'ngRoute',
  'weatherApp.services',
  'weatherApp.directives',
  'weatherApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/city-listing', {templateUrl: 'partials/city-listing.html', controller: 'weatherCtrl'});
  $routeProvider.when('/forecast/:cityname', {templateUrl: 'partials/daily-forecast.html', controller: 'weatherForcastCtrl'});
  $routeProvider.otherwise({redirectTo: '/city-listing'});
}]);
