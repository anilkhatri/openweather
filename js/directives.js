'use strict';

/* Directives */

angular.module('weatherApp.directives', [])
  
  .directive('weatherPanel',[function factory() {
    return {
      restrict: 'EA',
      scope: {
        useDayForecast: '=showEntry',
        forecast: '=weatherPanel'
      },
      templateUrl: 'partials/_daily-weather.html',
      link: function(scope, element, attrs) {
        scope.parseDate = function (time) {
          return new Date(time * 1000);
        };
      }
    }
  }])

  .directive('cityname',[function factory() {
  return {
    restrict: 'EA',   
    scope: {
      city: '=cityname'
    },
    templateUrl: 'partials/_city-weather.html',
    link: function(scope, element, attrs) {
    }
  }
}]);

