'use strict';

angular.module('weatherApp.controllers', [])

  .controller('weatherCtrl',
    ['$scope'
    ,'weatherMap'
    ,'exampleLocations',
    function($scope,weatherMap,exampleLocations) {
      $scope.message = '';
      $scope.hasState = '';
      $scope.exampleLocations = exampleLocations;
      $scope.getForecastByLocation = function() {
        if ($scope.exampleLocations.length == 0 || $scope.exampleLocations == undefined) {          
          return;
        }
        $scope.forecastbylocation = [];        
        $scope.exampleLocations.forEach (function (item, index) {
          $scope.forecastbylocation[index] = weatherMap.queryWeather({
            location: item
          });
        });
      };
      $scope.getForecastByLocation();
    }
  ])

  .controller('weatherForcastCtrl',
    ['$scope'
    ,'weatherMap',
    '$routeParams',
    function($scope,weatherMap,$routeParams) {
      
      $scope.location = $routeParams.cityname;
      console.log($scope.location)
      $scope.getDailyForecastByLocation = function() {

        if ($scope.location == '' || $scope.location == undefined) {          
          return;
        }

        $scope.forecast = weatherMap.queryForecastDaily({
          location: $scope.location
        });
      };

      $scope.getDailyForecastByLocation();
      
    }
  ])
